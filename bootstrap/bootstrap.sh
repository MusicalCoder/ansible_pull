#!/usr/bin/env bash

# # # # # CONFIG

# binaries
echo "\e[34mSetting up config variables...\e[0m"
PLAYBOOK="main.yaml"
SUDO="$(which sudo)"
APT="$(which apt)"
RM="$(which rm)"

# LOCATIONS
echo "\e[34mConfiguring working directories...\e[0m"
WORKING="/tmp"
REPO="${WORKING}/ansible_pull"
REPO_URL="https://gitlab.com/MusicalCoder/ansible_pull.git"

# # # # # MAIN

# install dependecies
echo "\e[34mInstalling dependencies...\e[36m"
${SUDO} ${APT} update
${SUDO} ${APT} install -y ansible git openssh-server python3-pip


# loading possibly installed binaries
echo "\e[34mSetting up additional config variables...\e[0m"
GIT="$(which git)"
AP="$(which ansible-playbook)"
AG="$(which ansible-galaxy)"
PIP="$(which pip)"

# move the ansible vault key file from the install medium to the computer
echo "\e[34mMoving ansible vault key to its use location...\e[0m"
if [ -f "ansible_vault_key" ]; then
    cp ./ansible_vault_key ~/.ansible_vault_key
    chmod 644 ~/.ansible_vault_key
else
    echo "\e[34mAnsible vault key is not found...\e[0m"
fi

# make our tmp install folder
echo "\e[34mMaking the throwaway location...\e[32m"
cd ${WORKING}
#mkdir ${VSCEXT}
${GIT} clone ${REPO_URL}
cd ${REPO}

# install ansible dependencies
echo "\e[34mInstalling ansible dependencies...\e[32m"
${AG} install -r requirements.yaml --ignore-errors --force
${PIP} install github3.py

# run our playbook
echo "\e[34m To install the playbook, simply run make with one of the following options...\e[0m"
make help
#${AP} --vault-password-file ~/.ansible_vault_key ./${PLAYBOOK} -c local -K 

# cleaning up
#echo "\e[31Cleaning up temporary locations...\e[32m"
#cd
#${RM} -rf ${REPO}
#${RM} -rf ${VSCEXT}

#echo "\e[31mAnsible Bootstrap is now complete...\e[0m"
exit 0
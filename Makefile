SHELL=/bin/bash

# CREDIT - Colour pulled from https://gist.github.com/rsperl/d2dfe88a520968fbc1f49db0a29345b9

# to see all colors, run
# bash -c 'for c in {0..255}; do tput setaf $c; tput setaf $c | cat -v; echo =$c; done'
# the first 15 entries are the 8-bit colors

# define standard colors
ifneq (,$(findstring xterm,${TERM}))
	BLACK        := $(shell tput -Txterm setaf 0)
	RED          := $(shell tput -Txterm setaf 1)
	GREEN        := $(shell tput -Txterm setaf 2)
	YELLOW       := $(shell tput -Txterm setaf 3)
	LIGHTPURPLE  := $(shell tput -Txterm setaf 4)
	PURPLE       := $(shell tput -Txterm setaf 5)
	BLUE         := $(shell tput -Txterm setaf 6)
	WHITE        := $(shell tput -Txterm setaf 7)
	RESET := $(shell tput -Txterm sgr0)
else
	BLACK        := ""
	RED          := ""
	GREEN        := ""
	YELLOW       := ""
	LIGHTPURPLE  := ""
	PURPLE       := ""
	BLUE         := ""
	WHITE        := ""
	RESET        := ""
endif

# set target color
TARGET_COLOR := $(BLUE)

POUND = \#

.DEFAULT_GOAL := help

PLAYBOOK = main.yaml

install: ## install software dependencies
	@echo "${BLUE}installing software dependencies ${RESET}"
	@sudo apt update
	@sudo apt install git ansible -y
	@echo "${BLUE}installing playbook dependencies ${RESET}"
	@ansible-galaxy install -r requirements.yaml --ignore-errors --force
run: ## run all ansible tasks - should only be run for testing or THE-DOCTOR
	@echo "${BLUE}running all ansible tasks${RESET}"
	@ansible-playbook --vault-password-file ~/.ansible_vault_key ./${PLAYBOOK} -c local -K 
personal: ## runs the following playbook tags: general * personal * using_vm * developer : so non-laptop PCs"
	@echo "${BLUE}running general/personal/using_vm/developer ansible tasks${RESET}"
	@ansible-playbook --vault-password-file ~/.ansible_vault_key --tags "general,personal,using_vm,developer" ./${PLAYBOOK} -c local -K 
laptop: ## runs the following playbook tags: general * personal * using_vm * developer * laptop - so my laptops
	@echo "${BLUE}running general/personal/using_vm/developer/laptop ansible tasks${RESET}"
	@ansible-playbook --vault-password-file ~/.ansible_vault_key --skip-tags "is_vm,office" ./${PLAYBOOK} -c local -K 
office: ## runs the following playbook tags: general * using_vm * developer * laptop * office - so my work laptop/pc
	@echo "${BLUE}running general/using_vm/developer/laptop/office ansible tasks${RESET}"
	@ansible-playbook --vault-password-file ~/.ansible_vault_key --skip-tags "personal,is_vm" ./${PLAYBOOK} -c local -K 
vm: ## runs the following playbook tags: general * developer * is_vm - so VMs used in testing
	@echo "${BLUE}running general/developer/is_vm ansible tasks${RESET}"
	@ansible-playbook --vault-password-file ~/.ansible_vault_key --tags "general,developer,is_vm" ./${PLAYBOOK} -c local -K 
test: ## run the playbook for a single tag: `make test TAG=[tag]` (case sensitive)
	@echo "${BLUE}testing ansible tag ${TAG} ${RESET}"
	@ansible-playbook --ask-become-pass --ask-vault-pass --connection=local --tags "${TAG}" "${PLAYBOOK}"
see_tags: ## run to see which tags are in use in the playbook
	@echo "${BLUE}retreived ansible tags ${RESET}"
	@grep -R 'tags:' . | grep -v '#' | less
help: ## this menu
	@echo ""
	@echo "    ${BLACK}:: ${RED}Makefile Help${RESET} ${BLACK}::${RESET}"
	@echo ""
	@echo "Available options are:"
	@echo "-----------------------------------------------------------------${RESET}"
	@grep -E '^[a-zA-Z_0-9%-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "${TARGET_COLOR}%-30s${RESET} %s\n", $$1, $$2}'

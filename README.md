# ansible_pull

my ansible pull repository

TODOs
- [x] install masterpdf
- [x] install latest zoom
- [x] install vivaldi
- [x] copy starting images
- [x] install gkeys (move to gkeys installer)
- [x] map other home subfolders to NAS
- [x] pull candy icons from github  [extract to ~/.icons]
- [x] pull sweet dark theme from github [extract to ~/.themes]
- [x] pull purple folders from github


programs to add
- [ ] kpartx - WORK
- [ ] snap - gitkraken (--classic) WORK


Tags
- general : this tag runs on EVERY ansible install
- personal : this tag will only run on my personal metal PCs (ie not vm's)
- using_vm : this tag will only run on pcs that will run VMs
- is_vm : this tag is will only run on VM installations (ie not using_vm - but actual VMs)
- developer : this tag will only run on machines using development
- laptop : this tag will only run on laptop machines
- office : this tag will only run for my office laptop

* future note - if satellite 5 ever comes back to linux - it needs mate-optimus installed